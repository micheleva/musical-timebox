## Credits
Initial code implementation from [Alexandre Beslic](https://github.com/abronan/heirloom) license [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

Sounds under the [Public Domain license](https://creativecommons.org/publicdomain/zero/1.0)


- getready.wav [Get_Ready_1.wav](https://freesound.org/people/MyMiniGemini/sounds/242727/)
- happy.wav: [8-bit Happy Ding](https://freesound.org/people/JapanYoshiTheGamer/sounds/361264/)
- rest.wav: [Blip Random](https://freesound.org/people/SpankMyFilth/sounds/215222/)

Sound under the [Attribution 3.0 unported license](https://creativecommons.org/licenses/by/3.0)

- done.wav: [Well done Mouse Squirrel Cartoon" by dersuperanton freesound.org](https://freesound.org/people/dersuperanton/sounds/436164/)

