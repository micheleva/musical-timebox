About the project
=================

`musical-timebox` is a barebone pomodoro server implementation for the Pomodoro® technique.

Requires `golang 1.19`. Tested only on Linux.

License
=======

GNU General Public License v3.0

See `COPYING` to see the full text.

Usage
=======

Run the server
```
$ musical-timebox # or `go run .`
```

Start the pomodoro from a different terminal with the following command
```
 $ curl <url>/start
```


Credits
=======
Thanks to Alexandre Beslic for the initial implementation.

Sounds from freesound.org: see `Credits.md` to see the full text.
