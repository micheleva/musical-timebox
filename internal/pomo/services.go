package pomo

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/micheleva/musical-timebox/internal/sound"
)

func pomodoroTurn(pomoCh chan bool, currentTurn *int, player sound.Player, config Config) {
	log.Printf("Please start working! (Pomodoro %v out of %v)\n", *currentTurn, config.Pomodoros)
	err := player.PlayASound(config.PomodoroSound)
	if err != nil {
		log.Println("Error playing audio:", err)
	}
	time.Sleep(config.PomodoroDuration)
	pomoCh <- true
}

func pomodoroBreak(breakCh chan bool, player sound.Player, config Config) {
	log.Println("Please rest!")
	err := player.PlayASound(config.ShortBreakSound)
	if err != nil {
		log.Println("Error playing audio:", err)
	}
	time.Sleep(config.ShortBreak)
	breakCh <- true
}

func pomodoroLongBreak(longBreakCh chan bool, player sound.Player, config Config) {
	log.Println("Please rest FOR A WHILE!")
	err := player.PlayASound(config.LongBreakSound)
	if err != nil {
		log.Println("Error playing audio:", err)
	}
	time.Sleep(config.LongBreak)
	longBreakCh <- true
}

func pomodoroTerminator(doneCh chan bool, isPomodoroGoing *bool, player sound.Player, config Config) {
	for {
		select {
		case weAreDone := <-doneCh:
			_ = weAreDone
			log.Println("We are done. Terminating this pomodoro.\nWe are now able to start a new pomodoro!")
			err := player.PlayASound(config.DoneSound)
			if err != nil {
				log.Println("Error playing audio:", err)
			}
			*isPomodoroGoing = false
		}
	}
}

func pomodoroService(pomoCh, breakCh, longBreakCh, doneCh chan bool, currentTurn *int, player sound.Player, config Config) {
	for {
		select {
		case pomo := <-pomoCh:
			_ = pomo
			// A pomodoro has completed. Figure out which kind of break do we need
			if *currentTurn >= config.Pomodoros {
				*currentTurn = 1
				go pomodoroLongBreak(longBreakCh, player, config)
			} else {
				*currentTurn += 1
				go pomodoroBreak(breakCh, player, config)
			}
		case breakShort := <-breakCh:
			_ = breakShort
			// A break has completed, start a new pomodoro
			_ = breakShort
			go pomodoroTurn(pomoCh, currentTurn, player, config)
		case breakLong := <-longBreakCh:
			_ = breakLong
			// We're done with all pomodoros. End the session (or ask for a new one?)
			fmt.Println("Hard work paid off well\nRest now, bask in your success\nCongratulations!")
			doneCh <- true
		}
	}
}

func CreatePomodoro(player sound.Player, currentTurn *int, isPomodoroGoing *bool, config Config) {
	pomoCh := make(chan bool)
	breakCh := make(chan bool)
	longBreakCh := make(chan bool)
	doneCh := make(chan bool)

	// Prevent concurrent pomodoros: only one at a time!
	*isPomodoroGoing = true

	// If the wait flag is set in the config, then give the user some time to prepare
	// before actually start the pomodoro!
	if config.IsWaitFlag {
		log.Println("Prepare: a pomodoro is being prepared for you...")
		err := player.PlayASound(config.GetReadySound)
		if err != nil {
			log.Println("Error playing audio:", err)
		}
		// Preparation time
		time.Sleep(config.WaitTimeAmount)
	}
	// Start the pomodoro
	go pomodoroService(pomoCh, breakCh, longBreakCh, doneCh, currentTurn, player, config)
	go pomodoroTerminator(doneCh, isPomodoroGoing, player, config)
	go pomodoroTurn(pomoCh, currentTurn, player, config)

}
