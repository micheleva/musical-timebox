package pomo

import "time"

type Config struct {
	PomodoroDuration time.Duration `mapstructure:"pomodoro_duration"`
	ShortBreak       time.Duration `mapstructure:"short_break_duration"`
	LongBreak        time.Duration `mapstructure:"long_break_duration"`
	Pomodoros        int           `mapstructure:"pomodoros"`
	PlayerCommand    string        `mapstructure:"player_command"`
	IsWaitFlag       bool          `mapstructure:"wait_for_user_to_be_ready"`
	WaitTimeAmount   time.Duration `mapstructure:"get_ready_duration"`
	GetReadySound    string        `mapstructure:"get_read_sound"`
	PomodoroSound    string        `mapstructure:"pomodoro_sound"`
	ShortBreakSound  string        `mapstructure:"short_break_sound"`
	LongBreakSound   string        `mapstructure:"long_break_sound"`
	DoneSound        string        `mapstructure:"done_sound"`
	CertFile         string        `mapstructure:"cert_file"`
	KeyFile          string        `mapstructure:"key_file"`
}
