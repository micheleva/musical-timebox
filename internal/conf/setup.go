package conf

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/micheleva/musical-timebox/internal/pomo"
)

func ConfigSetup(config *pomo.Config) {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configs")

	// Set sane defaults in case the config file is missing or malformed
	viper.SetDefault("port", "8080")
	viper.SetDefault("player_command", "aplay")

	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println("Config file not found, using default settings.\nNOTE: The sound player command was set to \"aplay\".")
		} else {
			log.Panicf("failed to read config file: %s", err)
		}
	}
	if err := viper.Unmarshal(&config); err != nil {
		log.Panicf("failed to read config file: %s", err)
	}
}
