package server

import (
	"log"
	"net/http"

	"gitlab.com/micheleva/musical-timebox/internal/pomo"
	"gitlab.com/micheleva/musical-timebox/internal/sound"
)

type Server struct {
	Addr            string
	config          pomo.Config
	CurrentTurn     *int
	IsPomodoroGoing *bool
	Mux             *http.ServeMux
	Player          sound.Player
	http.Server     // implicit interface implementation, do not forget to implement HandleFunc
}

func NewServer(port string, currentTurn *int, isPomodoroGoing *bool, player sound.Player, config pomo.Config) *Server {
	mux := http.NewServeMux()
	s := &Server{
		Addr:            ":" + port,
		config:          config,
		Mux:             mux,
		Player:          player,
		CurrentTurn:     currentTurn,
		IsPomodoroGoing: isPomodoroGoing,
	}
	mux.HandleFunc("/start", s.startPomodoro)

	return s
}

func (s *Server) Run() error {
	if s.config.CertFile != "" && s.config.KeyFile != "" {
		return http.ListenAndServeTLS(s.Addr, s.config.CertFile, s.config.KeyFile, s.Mux)
	} else {
		return http.ListenAndServe(s.Addr, s.Mux)
	}
}

func (s *Server) HandleFunc(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	s.Mux.HandleFunc(pattern, handler)
}

func (s *Server) startPomodoro(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if *s.IsPomodoroGoing {
		log.Println("Someone tried to execute a second pomodoro... and failed: only one pomodoro at the time!")
		errorBody := `{"result": "error","error_message":"A pomodoro is already ongoing, wait your turn please!"}`
		http.Error(w, errorBody, http.StatusTooManyRequests)
	} else {
		*s.IsPomodoroGoing = true
		// Start the pomodoro
		go pomo.CreatePomodoro(s.Player, s.CurrentTurn, s.IsPomodoroGoing, s.config)
		w.Write([]byte(`{"result": "success"}`))
	}
}
