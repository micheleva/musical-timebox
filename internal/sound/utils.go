package sound

import (
	"os/exec"
)

type Player interface {
	PlayASound(filename string) error
}

type DefaultPlayer struct {
	playerCommand string
}

func (dp DefaultPlayer) PlayASound(filename string) error {
	cmd := exec.Command(dp.playerCommand, filename)
	return cmd.Run()
}

func CreatePlayer(command string) DefaultPlayer {
	return DefaultPlayer{playerCommand: command}
}
