package main

import (
	"log"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/micheleva/musical-timebox/internal/conf"
	"gitlab.com/micheleva/musical-timebox/internal/pomo"
	"gitlab.com/micheleva/musical-timebox/internal/server"
	"gitlab.com/micheleva/musical-timebox/internal/sound"
)

var (
	config = pomo.Config{
		PomodoroDuration: time.Duration(25) * time.Minute,
		ShortBreak:       time.Duration(5) * time.Minute,
		LongBreak:        time.Duration(30) * time.Minute,
		Pomodoros:        7,
	}
	currentTurn     = 1
	isPomodoroGoing = false
	player          = sound.DefaultPlayer{}
)

func main() {
	conf.ConfigSetup(&config)
	player = sound.CreatePlayer(config.PlayerCommand)
	port := viper.GetString("port")

	server := server.NewServer(port, &currentTurn, &isPomodoroGoing, player, config)

	log.Println("Starting server on port", port)
	log.Fatal(server.Run())
}
